const yaml = require("js-yaml");
const css = require("@11tyrocks/eleventy-plugin-lightningcss");
const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");

const markdownIt = require("markdown-it");
const markdownItPandoc = require("markdown-it-pandoc");
let options = {
  html: true, // Enable HTML tags in source
  linkify: true,
  breaks: true,
};

// configure the library with options
let md = markdownIt(options).use(markdownItPandoc);
module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(css);
  // Return your Object options:
  //
  //add yaml for the site
  eleventyConfig.addDataExtension("yaml, yml", (contents) =>
    yaml.load(contents),
  );

  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);

  eleventyConfig.addShortcode("renderPokemon", function(poke) {
    return `<li>
${poke.nom} ${poke.type} ${poke.cri ? poke.cri : "meudh"} de la couleur ${poke.couleur_principale}
<img src="${poke.image}">
</li>`;
  });

  eleventyConfig.addFilter("markdownify", function(value) {
    if (!value) return "";
    if (typeof value !== "string") {
      console.log("markdownify is used for this:", value);
      return value;
    }
    return md.render(value);
  });

  eleventyConfig.addFilter("couleur", function(data, couleur) {
    console.log(data);
    console.log(couleur);

    return data.filter((a) => a.couleur_principale == couleur);
  });

  eleventyConfig.addFilter("markdownifyInline", function(value) {
    if (!value) return "";
    if (typeof value !== "string") {
      console.log("markdownify is used for this:", value);
      return value;
    }
    return md.renderInline(value);
  });

  eleventyConfig.addFilter("markownify");

  eleventyConfig.addCollection("lolz", function(collectionApi) {
    return collectionApi.getFilteredByGlob("src/content/lolz/*.md");
  });

  eleventyConfig.addPassthroughCopy({ "static/images": "images/" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "fonts/" });

  return {
    markdownTemplateEngine: "njk",
    dir: {
      includes: "layouts",
      data: "data",
      input: "src",
      output: "public",
    },
  };
};
